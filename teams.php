<?php
// Подключаемся к базе данных cs_tournament
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cs_tournament";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Если получен POST-запрос, выполняем соответствующую операцию с таблицей teams
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Получаем тип операции из скрытого поля action
    $action = $_POST["action"];

    if ($action == "create") {
        // Добавляем новую команду в таблицу teams
        $team_id = $_POST["team_id"];
        $team_name = $_POST["team_name"];
        $team_country = $_POST["team_country"];

        $sql = "INSERT INTO teams (team_id, team_name, team_country) VALUES ($team_id, '$team_name', '$team_country')";

        if ($conn->query($sql) === TRUE) {
            echo "New team added successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    } elseif ($action == "delete") {
        // Удаляем команду из таблицы teams по идентификатору
        $team_id = $_POST["team_id"];

        $sql = "DELETE FROM teams WHERE team_id = $team_id";

        if ($conn->query($sql) === TRUE) {
            echo "Team deleted successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    } elseif ($action == "edit") {
        // Редактируем команду в таблице teams по идентификатору
        $team_id = $_POST["team_id"];
        $team_name = $_POST["team_name"];
        $team_country = $_POST["team_country"];

        $sql = "UPDATE teams SET team_name = '$team_name', team_country = '$team_country' WHERE team_id = $team_id";

        if ($conn->query($sql) === TRUE) {
            echo "Team updated successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Teams Interface</title>
</head>
<body>
<h1>Teams Interface</h1>
<p>This is a simple interface for creating, viewing, deleting and editing teams in the cs_tournament database.</p>

<!-- Форма для ввода данных новой команды -->
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
    <h2>Create a new team</h2>
    <input type="hidden" name="action" value="create"> <!-- Скрытое поле для указания типа операции -->
    <label for="team_id">Team ID:</label>
    <input type="number" id="team_id" name="team_id" required><br>
    <label for="team_name">Team Name:</label>
    <input type="text" id="team_name" name="team_name" required><br>
    <label for="team_country">Team Country:</label>
    <input type="text" id="team_country" name="team_country" required><br>
    <input type="submit" value="Submit">
</form>

<!-- Таблица для отображения данных из таблицы teams -->
<table border="1">
    <h2>View all teams</h2>
    <tr>
        <th>Team ID</th>
        <th>Team Name</th>
        <th>Team Country</th>
        <th>Delete</th>
        <th>Edit</th>
    </tr>
    <?php
    // Выбираем все записи из таблицы teams
    $sql = "SELECT * FROM teams";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // Выводим данные каждой записи в таблице
        while($row = $result->fetch_assoc()) {
            echo "<tr>";
            echo "<td>" . $row["team_id"] . "</td>";
            echo "<td>" . $row["team_name"] . "</td>";
            echo "<td>" . $row["team_country"] . "</td>";
            // Добавляем кнопки для удаления и редактирования записи
            echo "<td><form method='post' action='" . $_SERVER['PHP_SELF'] . "'><input type='hidden' name='action' value='delete'><input type='hidden' name='team_id' value='" . $row["team_id"] . "'><input type='submit' value='Delete'></form></td>";
            echo "<td><form method='post' action='" . $_SERVER['PHP_SELF'] . "'><input type='hidden' name='action' value='edit'><input type='hidden' name='team_id' value='" . $row["team_id"] . "'><input type='text' name='team_name' value='" . $row["team_name"] . "'><input type='text' name='team_country' value='" . $row["team_country"] . "'><input type='submit' value='Edit'></form></td>";
            echo "</tr>";
        }
    } else {
        echo "No teams found";
    }
    ?>
</table>

<?php
// Закрываем соединение с базой данных
$conn->close();
?>
</body>
</html>
