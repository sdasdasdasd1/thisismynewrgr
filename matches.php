<?php
// Подключаемся к базе данных cs_tournament
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cs_tournament";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Если получен POST-запрос, выполняем соответствующую операцию с таблицей matches
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Получаем тип операции из скрытого поля action
    $action = $_POST["action"];

    if ($action == "create") {
        // Добавляем новый матч в таблицу matches
        $match_id = $_POST["match_id"];
        $match_date = $_POST["match_date"];
        $match_time = $_POST["match_time"];
        $match_map = $_POST["match_map"];
        $match_team1 = $_POST["match_team1"];
        $match_team2 = $_POST["match_team2"];

        $sql = "INSERT INTO matches (match_id, match_date, match_time, match_map, match_team1, match_team2) VALUES ($match_id, '$match_date', '$match_time', '$match_map', $match_team1, $match_team2)";

        if ($conn->query($sql) === TRUE) {
            echo "New match added successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    } elseif ($action == "delete") {
        // Удаляем матч из таблицы matches по идентификатору
        $match_id = $_POST["match_id"];

        $sql = "DELETE FROM matches WHERE match_id = $match_id";

        if ($conn->query($sql) === TRUE) {
            echo "Match deleted successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    } elseif ($action == "edit") {
        // Редактируем матч в таблице matches по идентификатору
        $match_id = $_POST["match_id"];
        $match_date = $_POST["match_date"];
        $match_time = $_POST["match_time"];
        $match_map = $_POST["match_map"];
        $match_team1 = $_POST["match_team1"];
        $match_team2 = $_POST["match_team2"];

        $sql = "UPDATE matches SET match_date = '$match_date', match_time = '$match_time', match_map = '$match_map', match_team1 = $match_team1, match_team2 = $match_team2 WHERE match_id = $match_id";

        if ($conn->query($sql) === TRUE) {
            echo "Match updated successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Matches Interface</title>
</head>
<body>
<h1>Matches Interface</h1>
<p>This is a simple interface for creating, viewing, deleting and editing matches in the cs_tournament database.</p>

<!-- Форма для ввода данных нового матча -->
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
    <h2>Create a new match</h2>
    <input type="hidden" name="action" value="create"> <!-- Скрытое поле для указания типа операции -->
    <label for="match_id">Match ID:</label>
    <input type="number" id="match_id" name="match_id" required><br>
    <label for="match_date">Match Date:</label>
    <input type="date" id="match_date" name="match_date" required><br>
    <label for="match_time">Match Time:</label>
    <input type="time" id="match_time" name="match_time" required><br>
    <label for="match_map">Match Map:</label>
    <input type="text" id="match_map" name="match_map" required><br>
    <label for="match_team1">Match Team 1:</label>
    <input type="number" id="match_team1" name="match_team1" required><br>
    <label for="match_team2">Match Team 2:</label>
    <input type="number" id="match_team2" name="match_team2" required><br>
    <input type="submit" value="Submit">
</form>

<!-- Таблица для отображения данных из таблицы matches -->
<table border="1">
    <h2>View all matches</h2>
    <tr>
        <th>Match ID</th>
        <th>Match Date</th>
        <th>Match Time</th>
        <th>Match Map</th>
        <th>Match Team 1</th>
        <th>Match Team 2</th>
        <th>Delete</th>
        <th>Edit</th>
    </tr>
    <?php
    // Выбираем все записи из таблицы matches
    $sql = "SELECT * FROM matches";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // Выводим данные каждой записи в таблице
        while($row = $result->fetch_assoc()) {
            echo "<tr>";
            echo "<td>" . $row["match_id"] . "</td>";
            echo "<td>" . $row["match_date"] . "</td>";
            echo "<td>" . $row["match_time"] . "</td>";
            echo "<td>" . $row["match_map"] . "</td>";
            echo "<td>" . $row["match_team1"] . "</td>";
            echo "<td>" . $row["match_team2"] . "</td>";
            // Добавляем кнопки для удаления и редактирования записи
            echo "<td><form method='post' action='" . $_SERVER['PHP_SELF'] . "'><input type='hidden' name='action' value='delete'><input type='hidden' name='match_id' value='" . $row["match_id"] . "'><input type='submit' value='Delete'></form></td>";
            echo "<td><form method='post' action='" . $_SERVER['PHP_SELF'] . "'><input type='hidden' name='action' value='edit'><input type='hidden' name='match_id' value='" . $row["match_id"] . "'><input type='date' name='match_date' value='" . $row["match_date"] . "'><input type='time' name='match_time' value='" . $row["match_time"] . "'><input type='text' name='match_map' value='" . $row["match_map"] . "'><input type='number' name='match_team1' value='" . $row["match_team1"] . "'><input type='number' name='match_team2' value='" . $row["match_team2"] . "'><input type='submit' value='Edit'></form></td>";
            echo "</tr>";
        }
    } else {
        echo "No matches found";
    }
    ?>
</table>

<?php
// Закрываем соединение с базой данных
$conn->close();
?>
</body>
</html>
