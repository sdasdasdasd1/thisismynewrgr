<?php
// Подключаемся к базе данных cs_tournament
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cs_tournament";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Если получен POST-запрос, выполняем соответствующую операцию с таблицей results
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Получаем тип операции из скрытого поля action
    $action = $_POST["action"];

    if ($action == "create") {
        // Добавляем новый результат в таблицу results
        $result_id = $_POST["result_id"];
        $result_match = $_POST["result_match"];
        $result_winner = $_POST["result_winner"];
        $result_score1 = $_POST["result_score1"];
        $result_score2 = $_POST["result_score2"];

        $sql = "INSERT INTO results (result_id, result_match, result_winner, result_score1, result_score2) VALUES ($result_id, $result_match, $result_winner, $result_score1, $result_score2)";

        if ($conn->query($sql) === TRUE) {
            echo "New result added successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    } elseif ($action == "delete") {
        // Удаляем результат из таблицы results по идентификатору
        $result_id = $_POST["result_id"];

        $sql = "DELETE FROM results WHERE result_id = $result_id";

        if ($conn->query($sql) === TRUE) {
            echo "Result deleted successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    } elseif ($action == "edit") {
        // Редактируем результат в таблице results по идентификатору
        $result_id = $_POST["result_id"];
        $result_match = $_POST["result_match"];
        $result_winner = $_POST["result_winner"];
        $result_score1 = $_POST["result_score1"];
        $result_score2 = $_POST["result_score2"];

        $sql = "UPDATE results SET result_match = $result_match, result_winner = $result_winner, result_score1 = $result_score1, result_score2 = $result_score2 WHERE result_id = $result_id";

        if ($conn->query($sql) === TRUE) {
            echo "Result updated successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Results Interface</title>
</head>
<body>
<h1>Results Interface</h1>
<p>This is a simple interface for creating, viewing, deleting and editing results in the cs_tournament database.</p>

<!-- Форма для ввода данных нового результата -->
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
    <h2>Create a new result</h2>
    <input type="hidden" name="action" value="create"> <!-- Скрытое поле для указания типа операции -->
    <label for="result_id">Result ID:</label>
    <input type="number" id="result_id" name="result_id" required><br>
    <label for="result_match">Result Match:</label>
    <input type="number" id="result_match" name="result_match" required><br>
    <label for="result_winner">Result Winner:</label>
    <input type="number" id="result_winner" name="result_winner" required><br>
    <label for="result_score1">Result Score 1:</label>
    <input type="number" id="result_score1" name="result_score1" required><br>
    <label for="result_score2">Result Score 2:</label>
    <input type="number" id="result_score2" name="result_score2" required><br>
    <input type="submit" value="Submit">
</form>

<!-- Таблица для отображения данных из таблицы results -->
<table border="1">
    <h2>View all results</h2>
    <tr>
        <th>Result ID</th>
        <th>Result Match</th>
        <th>Result Winner</th>
        <th>Result Score 1</th>
        <th>Result Score 2</th>
        <th>Delete</th>
        <th>Edit</th>
    </tr>
    <?php
    // Выбираем все записи из таблицы results
    $sql = "SELECT * FROM results";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // Выводим данные каждой записи в таблице
        while($row = $result->fetch_assoc()) {
            echo "<tr>";
            echo "<td>" . $row["result_id"] . "</td>";
            echo "<td>" . $row["result_match"] . "</td>";
            echo "<td>" . $row["result_winner"] . "</td>";
            echo "<td>" . $row["result_score1"] . "</td>";
            echo "<td>" . $row["result_score2"] . "</td>";
            // Добавляем кнопки для удаления и редактирования записи
            echo "<td><form method='post' action='" . $_SERVER['PHP_SELF'] . "'><input type='hidden' name='action' value='delete'><input type='hidden' name='result_id' value='" . $row["result_id"] . "'><input type='submit' value='Delete'></form></td>";
            echo "<td><form method='post' action='" . $_SERVER['PHP_SELF'] . "'><input type='hidden' name='action' value='edit'><input type='hidden' name='result_id' value='" . $row["result_id"] . "'><input type='number' name='result_match' value='" . $row["result_match"] . "'><input type='number' name='result_winner' value='" . $row["result_winner"] . "'><input type='number' name='result_score1' value='" . $row["result_score1"] . "'><input type='number' name='result_score2' value='" . $row["result_score2"] . "'><input type='submit' value='Edit'></form></td>";
            echo "</tr>";
        }
    } else {
        echo "No results found";
    }
    ?>
</table>

<?php
// Закрываем соединение с базой данных
$conn->close();
?>
</body>
</html>
