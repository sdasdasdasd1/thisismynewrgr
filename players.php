<?php
// Подключаемся к базе данных cs_tournament
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cs_tournament";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Если получен POST-запрос, выполняем соответствующую операцию с таблицей players
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Получаем тип операции из скрытого поля action
    $action = $_POST["action"];

    if ($action == "create") {
        // Добавляем нового игрока в таблицу players
        $player_id = $_POST["player_id"];
        $player_name = $_POST["player_name"];
        $player_nickname = $_POST["player_nickname"];
        $player_team = $_POST["player_team"];

        $sql = "INSERT INTO players (player_id, player_name, player_nickname, player_team) VALUES ($player_id, '$player_name', '$player_nickname', $player_team)";

        if ($conn->query($sql) === TRUE) {
            echo "New player added successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    } elseif ($action == "delete") {
        // Удаляем игрока из таблицы players по идентификатору
        $player_id = $_POST["player_id"];

        $sql = "DELETE FROM players WHERE player_id = $player_id";

        if ($conn->query($sql) === TRUE) {
            echo "Player deleted successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    } elseif ($action == "edit") {
        // Редактируем игрока в таблице players по идентификатору
        $player_id = $_POST["player_id"];
        $player_name = $_POST["player_name"];
        $player_nickname = $_POST["player_nickname"];
        $player_team = $_POST["player_team"];

        $sql = "UPDATE players SET player_name = '$player_name', player_nickname = '$player_nickname', player_team = $player_team WHERE player_id = $player_id";

        if ($conn->query($sql) === TRUE) {
            echo "Player updated successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Players Interface</title>
</head>
<body>
<h1>Players Interface</h1>
<p>This is a simple interface for creating, viewing, deleting and editing players in the cs_tournament database.</p>

<!-- Форма для ввода данных нового игрока -->
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
    <h2>Create a new player</h2>
    <input type="hidden" name="action" value="create"> <!-- Скрытое поле для указания типа операции -->
    <label for="player_id">Player ID:</label>
    <input type="number" id="player_id" name="player_id" required><br>
    <label for="player_name">Player Name:</label>
    <input type="text" id="player_name" name="player_name" required><br>
    <label for="player_nickname">Player Nickname:</label>
    <input type="text" id="player_nickname" name="player_nickname" required><br>
    <label for="player_team">Player Team:</label>
    <input type="number" id="player_team" name="player_team" required><br>
    <input type="submit" value="Submit">
</form>

<!-- Таблица для отображения данных из таблицы players -->
<table border="1">
    <h2>View all players</h2>
    <tr>
        <th>Player ID</th>
        <th>Player Name</th>
        <th>Player Nickname</th>
        <th>Player Team</th>
        <th>Delete</th>
        <th>Edit</th>
    </tr>
    <?php
    // Выбираем все записи из таблицы players
    $sql = "SELECT * FROM players";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // Выводим данные каждой записи в таблице
        while($row = $result->fetch_assoc()) {
            echo "<tr>";
            echo "<td>" . $row["player_id"] . "</td>";
            echo "<td>" . $row["player_name"] . "</td>";
            echo "<td>" . $row["player_nickname"] . "</td>";
            echo "<td>" . $row["player_team"] . "</td>";
            // Добавляем кнопки для удаления и редактирования записи
            echo "<td><form method='post' action='" . $_SERVER['PHP_SELF'] . "'><input type='hidden' name='action' value='delete'><input type='hidden' name='player_id' value='" . $row["player_id"] . "'><input type='submit' value='Delete'></form></td>";
            echo "<td><form method='post' action='" . $_SERVER['PHP_SELF'] . "'><input type='hidden' name='action' value='edit'><input type='hidden' name='player_id' value='" . $row["player_id"] . "'><input type='text' name='player_name' value='" . $row["player_name"] . "'><input type='text' name='player_nickname' value='" . $row["player_nickname"] . "'><input type='number' name='player_team' value='" . $row["player_team"] . "'><input type='submit' value='Edit'></form></td>";
            echo "</tr>";
        }
    } else {
        echo "No players found";
    }
    ?>
</table>

<?php
// Закрываем соединение с базой данных
$conn->close();
?>
</body>
</html>
